#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QFileDialog>
#include <QString>
#include <fstream>
#include <iostream>
#include <memory>
#include <sstream>
#include <algorithm>
#include <iterator>

using namespace OpenNN;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
    ui(new Ui::MainWindow),
    lista_de_clases{"hombre", "mujer", "sin-persona"},
    tam_imagenes{30, 30}
{
    ui->setupUi(this);

    // Connect button signal to appropriate slot
    connect(ui->createDataset, SIGNAL(released()), this, SLOT(generateDataset()));
    connect(ui->cargarDataset, SIGNAL(released()), this, SLOT(cargarDataset()));
    connect(ui->entrenarRed, SIGNAL(released()), this, SLOT(trainNetwork()));
    connect(ui->guardarRed, SIGNAL(released()), this, SLOT(guardarRed()));
    connect(ui->cargar_red, SIGNAL(released()), this, SLOT(cargarRed()));
    connect(ui->testNet, SIGNAL(released()), this, SLOT(testNetwork()));
    connect(ui->cargarImagen, SIGNAL(released()), this, SLOT(cargarImagen()));
    connect(ui->encontrarCaras, SIGNAL(released()), this, SLOT(detectarCaras()));
    connect(ui->guardarImagen, SIGNAL(released()), this, SLOT(guardarImagen()));

    connect(this, SIGNAL(textChanged(QString)), this, SLOT(onTextChanged(QString)));

    // Es escalan los contenidos de la imagen de las caras
    ui->imagenCaras->setScaledContents(true);
}

void MainWindow::generateDataset()
{
    // Obtener valores de la ventana
    tam_imagenes.x = ui->tam_fotos_spinbox->value();
    tam_imagenes.y = ui->tam_fotos_spinbox->value();

    qDebug() << "Generate Dataset";
    QString datasetDir = QString(QFileDialog::getExistingDirectory(this, "Select dataset folder"));

    // Archivo .csv en el que se van a guardar todas las imágenes de entrenamiento, además de la clase
    std::ofstream ofs(datasetDir.toStdString() + "/trainset.csv");

    if (!ofs.is_open()) {
        std::cerr << "Error al abrir el archivo trainset.csv" << std::endl;
        return;
    }

    // Se seleccionara una de estas funciones
    auto color_2_gris = [](const QRgb* const pixel_rgb){return qGray(*pixel_rgb);};
    auto color_2_rojo = [](const QRgb* const pixel_rgb){return (*pixel_rgb >> 16) & 0xFF;};
    auto color_2_verde = [](const QRgb* const pixel_rgb){return (*pixel_rgb >> 8) & 0xFF;};
    auto color_2_azul = [](const QRgb* const pixel_rgb){return (*pixel_rgb >> 0) & 0xFF;};

    // Todas las fotos se van a juntar y guardar en un archivo CSV, para poder leerlo con la clase DataSet. 
    size_t clase_id = 0;
    for (const std::string& clase : lista_de_clases) {

        // Se leen los nombres de todas las imágenes de la clase
        QStringList ruta_imagenes = QDir(datasetDir + "/" + clase.c_str()).entryList(QStringList() << "*.jpg" << "*.JPG", QDir::Files);

        std::cout << "Leyendo las " << ruta_imagenes.size() << " imagenes en la carpeta " << clase << "..." << std::endl;
        for (const auto& ruta_imagen : ruta_imagenes) {
            // Se carga la imagen
            QImage imagen(datasetDir + "/" + clase.c_str() + "/" + ruta_imagen);

            // Si ha habido algún problema leyendo la imagen, continuar con la siguiente
            if (imagen.isNull()){
                std::cout << "Error al abrir la imagen " << ruta_imagen.toStdString() << std::endl;
                continue;
            }

            // Redimensionar la imagen, para que el dataset ocupe menos espacio y cargue mas rapido
            imagen = imagen.scaled(tam_imagenes.x, tam_imagenes.y);

            // Los primeros 3 valores de cada fila indican la clase de la foto
            for (size_t it = 0; it < 3; ++it) {
                ofs << (( clase_id == it ) ? 1 : 0);
                if (it < 2)
                    ofs << " ";
            }

            // Se calcula su conversion a blanco y negro pixel a pixel y se guarda en el CSV
            for (int i = 0; i < imagen.height(); ++i) {
                // Se lee la imagen por filas (más rápido que pixel a píxel)   
                uchar* fila = imagen.scanLine(i);
                const int canales = 4;
                // Por cada pixel de la fila, convertir a escala de grises y guardarlo en el CSV
                for (int j = 0; j < imagen.width(); ++j) {
                    QRgb* pixel_rgb = reinterpret_cast<QRgb*>(fila + j*canales);
                    if (ui->colorSelection->currentText() == tr("QGray"))
                        ofs << " " << color_2_gris(pixel_rgb);
                    else if (ui->colorSelection->currentText() == tr("Rojo"))
                        ofs << " " << color_2_rojo(pixel_rgb);
                    else if (ui->colorSelection->currentText() == tr("Verde"))
                        ofs << " " << color_2_verde(pixel_rgb);
                    else if (ui->colorSelection->currentText() == tr("Azul"))
                        ofs << " " << color_2_azul(pixel_rgb);
                    else {
                        std::cout << "ERROR: Modo de conversion a gris incorrecto" << std::endl;
                        exit(-1);
                    }

                }
            } 

            // Se deja paso a la siguiente imagen
            ofs << std::endl;
        }

        clase_id++;
    }

    std::cout << "Archivo trainset.csv generado correctamente." << std::endl;
}

void MainWindow::cargarDataset()
{
    // Se selecciona un archivo CSV que contiene el dataset a ser usado
    QString ruta_dataset = QString(QFileDialog::getOpenFileName(this, "Selecciona el archivo del dataset"));

    std::cout << "Creando dataset..." << std::endl;

    // Cargar dataset
    dataset_ = std::make_shared<OpenNN::DataSet>();
    dataset_->set_data_file_name(ruta_dataset.toStdString());
    dataset_->set_separator("Space");
    dataset_->load_data();
    
    OpenNN::Variables* variables_p = dataset_->get_variables_pointer();
    variables_p->set_name(0, "hombre");
    variables_p->set_use(0, OpenNN::Variables::Target);
    variables_p->set_name(1, "mujer");
    variables_p->set_use(1, OpenNN::Variables::Target);
    variables_p->set_name(2, "sin-persona");
    variables_p->set_use(2, OpenNN::Variables::Target);
    for (size_t i = 3; i < tam_imagenes.x * tam_imagenes.y + 3; ++i)
        variables_p->set_use(i, OpenNN::Variables::Input);

    informacion_entradas_ = std::make_shared<OpenNN::Matrix<std::string>>(variables_p->arrange_inputs_information());
    informacion_targets_ = std::make_shared<OpenNN::Matrix<std::string>>(variables_p->arrange_targets_information());

    // Dividir el dataset en train (60%), validation (20%) y test (20%), de forma aleatoria
    OpenNN::Instances* instancia_p = dataset_->get_instances_pointer();
    instancia_p->split_random_indices(static_cast<float>(ui->entr_spinbox->value()) / 100.0f,
                                      static_cast<float>(ui->valid_spinbox_2->value()) / 100.0f,
                                      static_cast<float>(ui->test_spinbox->value()) / 100.0f);

    // Se normalizan las entradas para que la red funcione mejor
    estadisticas_entradas_ = std::make_shared<OpenNN::Vector<OpenNN::Statistics<double>>>(dataset_->scale_inputs_minimum_maximum());

    std::cout << "Dataset creado!" << std::endl;
}

void MainWindow::trainNetwork()
{
    if (!dataset_) {
        std::cout << "ERROR: No se ha creado el dataset!" << std::endl;
        return;
    }

    // Se pone la semilla de generación de números aleatorios con el valor del reloj del ordenador,
    // para que entre otras cosas la inicialización de los pesos sea diferente
    std::srand(static_cast<unsigned>(std::time(nullptr)));

    /****************
    *  Red neural  *
    ****************/

    // Definir la arquitectura
    const size_t n_capas = ui->n_capas_spinbox->value();
    const size_t n_neuronas_por_capa = ui->n_neuronas_por_capa_spinbox->value();
    
    OpenNN::Vector<size_t> arquitectura(n_capas);
    arquitectura[0] = tam_imagenes.x * tam_imagenes.y;
    for (size_t i = 1; i < n_capas-1; ++i) {
        arquitectura[i] = n_neuronas_por_capa;
    }
    arquitectura[n_capas-1] = 3;

    // Crear la red neuronal
    red_neuronal_ = std::make_shared<OpenNN::NeuralNetwork>(arquitectura);

    red_neuronal_->get_inputs_pointer()->set_information(*informacion_entradas_);
    red_neuronal_->get_outputs_pointer()->set_information(*informacion_targets_);

    red_neuronal_->construct_scaling_layer();
    OpenNN::ScalingLayer* scaling_layer_p = red_neuronal_->get_scaling_layer_pointer();
    scaling_layer_p->set_statistics(*estadisticas_entradas_);
    scaling_layer_p->set_scaling_method(OpenNN::ScalingLayer::ScalingMethod::NoScaling);

    // Activacion de las capas internas
    auto f_act_mlp_str = ui->f_act_int_combo_box->currentText();
    OpenNN::NeuralNetwork nn;
    //nn.get_multilayer_perceptron_pointer()->get_
    if (f_act_mlp_str == tr("Linear")) {
        OpenNN::Vector<OpenNN::Perceptron::ActivationFunction> act(n_capas-1, OpenNN::Perceptron::ActivationFunction::Linear);
        red_neuronal_->get_multilayer_perceptron_pointer()->set_layers_activation_function(act);
    } else if (f_act_mlp_str == tr("Logistic")) {
        OpenNN::Vector<OpenNN::Perceptron::ActivationFunction> act(n_capas-1, OpenNN::Perceptron::ActivationFunction::Logistic);
        red_neuronal_->get_multilayer_perceptron_pointer()->set_layers_activation_function(act);
    } else if (f_act_mlp_str == tr("H. Tangent")) {
        OpenNN::Vector<OpenNN::Perceptron::ActivationFunction> act(n_capas-1, OpenNN::Perceptron::ActivationFunction::HyperbolicTangent);
        red_neuronal_->get_multilayer_perceptron_pointer()->set_layers_activation_function(act);
    } else if (f_act_mlp_str == tr("Threshold")) {
        OpenNN::Vector<OpenNN::Perceptron::ActivationFunction> act(n_capas-1, OpenNN::Perceptron::ActivationFunction::Threshold);
        red_neuronal_->get_multilayer_perceptron_pointer()->set_layers_activation_function(act);
    } else if (f_act_mlp_str == tr("Sym. Threshold")) {
        OpenNN::Vector<OpenNN::Perceptron::ActivationFunction> act(n_capas-1, OpenNN::Perceptron::ActivationFunction::SymmetricThreshold);
        red_neuronal_->get_multilayer_perceptron_pointer()->set_layers_activation_function(act);
    } else {
        std::cout << "ERROR: No se ha especificado una funcion de activación del MLP adecuada" << std::endl;
        exit(-1);
    }

    // Activación de la última capa
    red_neuronal_->construct_probabilistic_layer();
    OpenNN::ProbabilisticLayer* capa_prob_p = red_neuronal_->get_probabilistic_layer_pointer();
    auto f_act_str = ui->f_act_combo_box_2->currentText(); 
    auto f_act = OpenNN::ProbabilisticLayer::Softmax;
    if (f_act_str == tr("Softmax")) {
        f_act = OpenNN::ProbabilisticLayer::Softmax;
    } else if (f_act_str == tr("No Probabilistic")) {
        f_act = OpenNN::ProbabilisticLayer::NoProbabilistic;
    } else if (f_act_str == tr("Binary")) {
        f_act = OpenNN::ProbabilisticLayer::Binary;
    } else if (f_act_str == tr("Competitive")) {
        f_act = OpenNN::ProbabilisticLayer::Competitive;
    } else if (f_act_str == tr("Probability")) {
        f_act = OpenNN::ProbabilisticLayer::Probability;
    } else {
        std::cout << "ERROR: No se ha especificado una funcion de activación de la capa probabilistica adecuada" << std::endl;
        exit(-1);
    }
    capa_prob_p->set_probabilistic_method(f_act);

    /*********************************
    *  Estrategia de entrenamiento  *
    *********************************/

    OpenNN::LossIndex id_perdida;
    id_perdida.set_data_set_pointer(dataset_.get());
    id_perdida.set_neural_network_pointer(red_neuronal_.get());

    // Regularizacion
    auto ref_str = ui->regularizacion_combobox->currentText();
    if (ref_str == tr("Sin regularizacion")) {
        id_perdida.set_regularization_type(OpenNN::LossIndex::NO_REGULARIZATION);
    } else if (ref_str == tr("Norma de los parametros")) {
        id_perdida.set_regularization_type(OpenNN::LossIndex::NEURAL_PARAMETERS_NORM);
    }

    // Función de pérdida
    auto loss_str = ui->f_loss_comboBox->currentText();
    if (loss_str == tr("Sum squared")) {
        id_perdida.set_error_type(OpenNN::LossIndex::SUM_SQUARED_ERROR);
    } else if (loss_str == tr("Mean Squared Error")) {
        id_perdida.set_error_type(OpenNN::LossIndex::MEAN_SQUARED_ERROR);
    } else if (loss_str == tr("Root Mean Squared Error")) {
        id_perdida.set_error_type(OpenNN::LossIndex::ROOT_MEAN_SQUARED_ERROR);
    } else if (loss_str == tr("Cross Entropy")) {
        id_perdida.set_error_type(OpenNN::LossIndex::CROSS_ENTROPY_ERROR);
    } else if (loss_str == tr("Normalized Squared Error")) {
        id_perdida.set_error_type(OpenNN::LossIndex::NORMALIZED_SQUARED_ERROR);
    } else {
        std::cout << "ERROR: Función de pérdida no es correcta" << std::endl;
        exit(-1);
    }

    // Coger el learning rate y número de iteraciones
    auto lr = ui->learning_rate_doubleSpinBox->value();
    auto epochs = ui->numero_iteraciones_spinbox->value();
    const float max_time = 1 * 60 * 60;

    OpenNN::TrainingStrategy estrategia;
    estrategia.set(&id_perdida);
    auto optim_str = ui->optim_combobox->currentText();
    if (optim_str == tr("Conjugate Gradient")) {
        estrategia.set_main_type(OpenNN::TrainingStrategy::CONJUGATE_GRADIENT);
        OpenNN::ConjugateGradient* cg_p = estrategia.get_conjugate_gradient_pointer();
        //cg_p->get_training_rate_algorithm_pointer()->set_training_rate_tolerance(lr);
        cg_p->set_error_training_rate(lr);
        cg_p->set_display_period(5);
        cg_p->set_maximum_iterations_number(epochs);
        cg_p->set_display(true);
        cg_p->set_maximum_time(max_time);
    } else if (optim_str == tr("Gradient Descent")) {
        estrategia.set_main_type(OpenNN::TrainingStrategy::GRADIENT_DESCENT);
        OpenNN::GradientDescent* gd_p = estrategia.get_gradient_descent_pointer();
        //gd_p->get_training_rate_algorithm_pointer()->set_training_rate_tolerance(lr);
        gd_p->set_error_training_rate(lr);
        gd_p->set_display_period(5);
        gd_p->set_maximum_iterations_number(epochs);
        gd_p->set_display(true);
        gd_p->set_maximum_time(max_time);
    } else if (optim_str == tr("Levenberg Marquardt")) {
        estrategia.set_main_type(OpenNN::TrainingStrategy::LEVENBERG_MARQUARDT_ALGORITHM);
        OpenNN::LevenbergMarquardtAlgorithm* lm_p = estrategia.get_Levenberg_Marquardt_algorithm_pointer();
        lm_p->set_display_period(5);
        lm_p->set_maximum_iterations_number(epochs);
        //lm_p->set_display(true); // No está implementado??
        lm_p->set_maximum_time(max_time);
    } else if (optim_str == tr("Newton Method")) {
        estrategia.set_main_type(OpenNN::TrainingStrategy::NEWTON_METHOD);
        OpenNN::NewtonMethod* nm_p = estrategia.get_Newton_method_pointer();
        //nm_p->get_training_rate_algorithm_pointer()->set_training_rate_tolerance(lr);
        nm_p->set_display_period(5);
        nm_p->set_maximum_iterations_number(epochs);
        nm_p->set_display(true);
        nm_p->set_maximum_time(max_time);
    } else if (optim_str == tr("Quasi Newton")) {
        estrategia.set_main_type(OpenNN::TrainingStrategy::QUASI_NEWTON_METHOD);
        OpenNN::QuasiNewtonMethod* qn_p = estrategia.get_quasi_Newton_method_pointer();
        //qn_p->get_training_rate_algorithm_pointer()->set_training_rate_tolerance(lr);
        qn_p->set_display_period(5);
        qn_p->set_maximum_iterations_number(epochs);
        qn_p->set_display(true);
        qn_p->set_maximum_time(max_time);
    } else {
        std::cout << "ERROR: Optimizador incorrecto" << std::endl;
        exit(-1);
    }
    
    /*******************
    *  Entrenamiento  *
    *******************/

    std::cout << "Entrenando la red neuronal..." << std::endl;
    const OpenNN::TrainingStrategy::Results resultados = estrategia.perform_training();
    std::cout << "Red neuronal entrenada!" << std::endl;
}

void MainWindow::guardarRed()
{
    if (red_neuronal_) {
        QString ruta = QString(QFileDialog::getSaveFileName(this, "Selecciona el archivo en el que guardar la red"));
        red_neuronal_->save(ruta.toStdString());
        std::cout << "Red guardada correctamente" << std::endl;
    } else {
        std::cout << "La red neuronal no existe!" << std::endl;
    }
}

void MainWindow::cargarRed()
{
    QString ruta = QString(QFileDialog::getOpenFileName(this, "Selecciona el archivo"));
    std::cout << ruta.toStdString() << std::endl;
    if (not red_neuronal_)
        red_neuronal_ = std::make_shared<OpenNN::NeuralNetwork>();

    red_neuronal_->load(ruta.toStdString());
}

void MainWindow::testNetwork()
{
    OpenNN::TestingAnalysis analisis(red_neuronal_.get(), dataset_.get());
    OpenNN::Matrix<size_t> confusion = analisis.calculate_confusion();

    // Guardar la matriz de confusion
    confusion.save("./redes-entrenadas/confusion.dat");
    float p = confusion.calculate_trace()/confusion.calculate_sum();

    std::stringstream ss; ss << p << "%";
    ui->precision->setText(ss.str().c_str());
    ss.clear();

    confusion.print();
    std::cout << std::endl;
}

void MainWindow::cargarImagen(void)
{
    // Elegir la imagen
    QString ruta = QString(QFileDialog::getOpenFileName(this, "Selecciona el archivo"));
    // Si se ha elegido una ruta
    if (not ruta.isEmpty()) {
        // Eliminar la imagen anterior y cargar la nueva imagen
        img_caras_.reset(new QImage(ruta));
        // Poner la imagen en la aplicación
        ui->imagenCaras->setPixmap(QPixmap::fromImage(*img_caras_.get()).scaled(ui->imagenCaras->size(),
                Qt::KeepAspectRatio, Qt::SmoothTransformation));
        // Aunque se muestra la imagen original en GUI, internamente se reescala la imagen para que
        // sea mas fácil trabajar con ella
        img_caras_.reset(new QImage(img_caras_->scaled(224, 168)));
    }
}

void MainWindow::detectarCaras(void)
{
    if (not red_neuronal_ || not img_caras_) {
        std::cout << "ERROR: Red neuronal o imagen no cargadas" << std::endl;
        return;
    }

    // Para cada pixel de la imagen, se tiene la cuenta de las votaciones de cada ventana en la que
    // esté incluido
    std::vector<std::vector<std::array<int, 3>>> resultados_pixeles { 224, { 168, {0, 0, 0} } };
    // Porción de la imagen que se usa como ventana
    QImage ventana;
    // Entradas a la red neuronal
    OpenNN::Vector<double> entradas_red;
    // Resultados de la red neuronal
    OpenNN::Vector<double> resultados_red;

    // Se desliza pixel a pixel la ventana por la imagen
    for (int i = 0; i < img_caras_->width() - tam_imagenes.x; ++i) {
        for (int j = 0; j < img_caras_->height() - tam_imagenes.y; ++j) {
            // Copiar la sección correspondiente de la imagen
            ventana = img_caras_->copy(i, j, tam_imagenes.x, tam_imagenes.y);
            // Preparar las entradas para la red neuronal (color -> blanco y negro)
            entradas_red = prepararEntrada(ventana);
            // Obtener las predicciones para la ventana actual
            resultados_red = red_neuronal_->calculate_outputs(entradas_red);
            // Se saca la predicción
            int resultado_actual = resultados_red.calculate_maximal_index();
            // Se aumentan los contadores
            for (size_t a = i; a < i + tam_imagenes.x; ++a) {
                for (size_t b = j; b < j + tam_imagenes.y; ++b) {
                    resultados_pixeles.at(a).at(b).at(resultado_actual)++;
                }
            }
        }
    }

    // Se pintan los resultados
    for (int i = 0; i < img_caras_->width(); ++i) {
        for (int j = 0; j < img_caras_->height(); ++j) {
            // Referencia al pixel actual
            auto& pixel = resultados_pixeles.at(i).at(j);
            // Obtener la clase más dominante en este pixel
            int clase = std::distance(pixel.begin(), std::max_element(pixel.begin(), pixel.end()));
            // Pintar el pixel de la imagen si ha encontrado una cara
            if (clase < 2) {
                if (clase == 0) {
                    img_caras_->setPixelColor(i, j, QColor(Qt::blue));
                } else if (clase == 1) {
                    img_caras_->setPixelColor(i, j, QColor(Qt::red));
                }
            }
        }
    }

    // Mostrar la imagen
    ui->imagenCaras->setPixmap(QPixmap::fromImage(*img_caras_.get()).scaled(ui->imagenCaras->size(),
            Qt::KeepAspectRatio, Qt::SmoothTransformation));
}

OpenNN::Vector<double> MainWindow::prepararEntrada(QImage& imagen) 
{
    OpenNN::Vector<double> resultado;
    // Se seleccionara una de estas funciones
    auto color_2_gris = [](const QRgb* const pixel_rgb){return qGray(*pixel_rgb);};
    auto color_2_rojo = [](const QRgb* const pixel_rgb){return (*pixel_rgb >> 16) & 0xFF;};
    auto color_2_verde = [](const QRgb* const pixel_rgb){return (*pixel_rgb >> 8) & 0xFF;};
    auto color_2_azul = [](const QRgb* const pixel_rgb){return (*pixel_rgb >> 0) & 0xFF;};
    
    for (int i = 0; i < imagen.height(); ++i) {
        // Se lee la imagen por filas (más rápido que pixel a píxel)   
        uchar* fila = imagen.scanLine(i);
        const int canales = 4;
        for (int j = 0; j < imagen.width(); ++j) {
            QRgb* pixel_rgb = reinterpret_cast<QRgb*>(fila + j*canales);
            size_t color;
            if (ui->colorSelection->currentText() == tr("QGray"))
                color = color_2_gris(pixel_rgb);
            else if (ui->colorSelection->currentText() == tr("Rojo"))
                color = color_2_rojo(pixel_rgb);
            else if (ui->colorSelection->currentText() == tr("Verde"))
                color = color_2_verde(pixel_rgb);
            else if (ui->colorSelection->currentText() == tr("Azul"))
                color = color_2_azul(pixel_rgb);
            else {
                std::cout << "ERROR: Modo de conversion a gris incorrecto" << std::endl;
                exit(-1);
            }
            double color_norm = (static_cast<double>(color) - 127.0) / 127.0;
            resultado.push_back(color_norm);
        }
    } 

    return resultado;
}

void MainWindow::guardarImagen()
{
    if (not img_caras_)
        return;

    QString ruta = QString(QFileDialog::getSaveFileName(this, "Selecciona donde guardar la imagen"));
    if (img_caras_->save(ruta)) {
        std::cout << "Imagen guardada correctamente" << std::endl;
    } else {
        std::cout << "ERROR: No se puedo guardar la imagen" << std::endl;
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}


QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    mainwindow.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

QMAKE_CXXFLAGS += -std=c++11 -fopenmp -pthread -lgomp
QMAKE_LFLAGS += -fopenmp -pthread -lgomp -lboost_filesystem
LIBS += -fopenmp -pthread -lgomp

INCLUDEPATH += $$PWD/../opennn
DEPENDPATH += $$PWD/../opennn
LIBS += -L$$OUT_PWD/../opennn/ -lopennn
PRE_TARGETDEPS += $$OUT_PWD/../opennn/libopennn.a
# LIBS += -L/home/emartine/Escritorio/sistemas-inteligentes/build/opennn -L/home/emartine/Escritorio/sistemas-inteligentes/tinyxml2 -lopennn -ltinyxml2


LIBS += -L$$OUT_PWD/../tinyxml2/ -ltinyxml2

INCLUDEPATH += $$PWD/../tinyxml2
DEPENDPATH += $$PWD/../tinyxml2

PRE_TARGETDEPS += $$OUT_PWD/../tinyxml2/libtinyxml2.a

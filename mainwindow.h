#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include "../opennn/opennn.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

// A cada clase se le asocia un numero de 0 a N
enum class Clase : size_t {
    HOMBRE = 0,
    MUJER,
    SIN_PERSONA
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void generateDataset();
    void cargarDataset();
    void trainNetwork();
    void guardarRed();
    void cargarRed();
    void testNetwork();
    void cargarImagen();
    void detectarCaras();
    void guardarImagen();

private:
    Ui::MainWindow *ui;

    // Nombre de las clases a clasificar por la red neuronal, en el orden correspondiente
    const std::vector<std::string> lista_de_clases;
    // Tamaño de las imagenes
    struct {
        size_t x, y;
    } tam_imagenes; 

    // Dataset
    std::shared_ptr<OpenNN::DataSet> dataset_;
    std::shared_ptr<OpenNN::Matrix<std::string>> informacion_targets_;
    std::shared_ptr<OpenNN::Matrix<std::string>> informacion_entradas_;
    std::shared_ptr<OpenNN::Vector<OpenNN::Statistics<double>>> estadisticas_entradas_;
    // Red neuronal
    std::shared_ptr<OpenNN::NeuralNetwork> red_neuronal_;

    // Imagen sobre la que detectar caras
    std::unique_ptr<QImage> img_caras_;

    // Convierte una imagen en un vector listo para usarse por la red neuronal
    OpenNN::Vector<double> prepararEntrada(QImage& imagen);
};

#endif // MAINWINDOW_H
